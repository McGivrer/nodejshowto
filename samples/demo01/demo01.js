var http = require('http');

function myRequestHandler(request,response){
	console.log(request.url+"is requested");
	response.writeHeader(200,{ 'Content-Type':"text/plain"});
	response.end("Hello World !",'utf8');
}

var server = http.createServer(myRequestHandler).listen(8000);