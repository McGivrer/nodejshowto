var http = require('http'),
    fs = require('fs'),
    url = require('url'),
    path = require('path'),
    mustache = require('mustache'),    
    textile = require('textile-js');

/**
 * Chargement d'un fichier <code>filePath</code>
 */
function loadFile(filePath,onSuccess,onError){
  fs.exists(filePath,function(exists){  
      if(!exists){
        onError(filePath);
      } else {
        fs.readFile(filePath, "utf8", function(err, data){
          if(err){
            onError(filePath);
          }else{
            onSuccess(filePath,data);
          }
      });
    }
    });
};

// Server request handler
function myRequestHandler(request,response){
  // si la page cherché est bien dans le path wiki
  var urlPath = url.parse(request.url).pathname;
  // si le fichier est bien dans /wiki
  if(urlPath == "/" ){
    // redirect to the wiki Home page
    response.writeHead(302, {
      'Location': "/wiki/index.textile"
    });
    response.end();
  }else if(urlPath.indexOf("/wiki") === 0){
    // on procède à son chargement
    var txtFilePath = path.join(process.cwd(), urlPath);

    loadFile( txtFilePath, 

      // onSuccess
      function(filepath,data){
        // Si le fichier demandé est bien une resource textile
        if(txtFilePath.match(".textile$") == ".textile"){
          // on convertit le textile en html
          var content = textile(data).toString();
          // on envoie la page générée sur la réponse
          decorateHtml(response, content);
        }else{
          // pas un fichier textile !
          sendFile(response, filepath, data);
        }
      },

      // onError
      function(path){
        sendHttpError(response, 404,"url '"+request.url+"' not found");
      }
    );  

  }else{

    // il n'y a pas d'abonné à l'adresse demandée
    sendHttpError(response, 404,"url '"+request.url+"' not found");
  }
};

/**
 * Génération d'une page d'erreur avec status HTTP = <code>errCode</code>
 * et retourne le message <code>errMessage</code>.
 */
function sendHttpError(response, errCode,errMessage){
  response.writeHeader(errCode,{'Content-Type':"text/html"});
  content="<h1>Error "+errCode+"</h1><p class=\"error\">"+errMessage+"</p>";
  decorateHtml(response, content);
};

/**
 * Ajoute la structure HTML au <code>content</code>.
 * @param content Contenu textile généré en HTML.
 */
function decorateHtml(response, content){
    loadFile(path.join(process.cwd(),"/wiki/template02-3.html"),
      function(path,template){
        // la réponse contiendra du HTML
        response.writeHeader(200,{'Content-Type':"text/html"});
        response.write(mustache.render(template,{'content':content}),'utf8');
        response.end();
      },
      function(path){
        // la réponse contiendra du HTML
        response.writeHeader(500,{'Content-Type':"text/html"});
        response.write("<h1>Error 500</h1>"
          +"<p class=\"\">Template ["+path+"] not found !</p>"
          + "<div class=\"container\"><div id=\"markup\"><div id=\"content\" class=\"markdown-body\">"
          + content
          + "</div></div></div>",'utf8');
        response.end();
      }); 
}

/** 
 * Send <code>data</code> from file <code>filepath</code> as
 * binary content to <code>response</code>. if <code>filepath</code>
 * contains ".css", set the Header mimes type to text/css.
 *
 * @param response the Http Response where to write the data.
 * @param data content data of the filepath file.
 */
function sendFile(response, filepath, data) {

  if (filepath.match(".css$") == ".css" ||
      filepath.match(".js$") == ".js" ) {
    response.writeHeader(200, { "Content-Type": "text/css" });
    response.write(data, "utf8");
  }else{
    response.write(data, "binary");
  }
  response.end();
}

// Déclaration du serveur
var server = http.createServer(myRequestHandler).listen(8000);