var http = require('http'),
    fs = require('fs'),
    url = require('url'),
    path = require('path'),
    mustache = require('mustache'),    
    textile = require('textile-js');

/**
 * Chargement d'un fichier <code>filePath</code>
 */
function loadFile(filePath,onSuccess,onError){
  fs.exists(filePath,function(exists){  
      if(!exists){
        onError(filePath);
      } else {
        fs.readFile(filePath, "utf8", function(err, data){
          if(err){
            onError(filePath);
          }else{
            onSuccess(filePath,data);
          }
      });
    }
    });
};

// Server request handler
function myRequestHandler(request,response){
  // si la page cherché est bien dans le path wiki
  var urlPath = url.parse(request.url).pathname;
  // si le fichier est bien dans /wiki
  if(urlPath.indexOf("/wiki") === 0){
    // on procède à son chargement
    var txtFilePath = path.join(process.cwd(), urlPath);
    // Si le fichier demandé est bien une resource textile
    if(txtFilePath.match("textile$") == "textile"){
      loadFile( txtFilePath, 
        // onSuccess
        function(path,data){
          // la réponse contiendra du HTML
          response.writeHeader(200,{'Content-Type':"text/html"});
          // on vconvertit le textile en html
          var content = textile(data).toString();
          // on envoie la page générée sur la réponse
          response.write(decorateHtml(content),'utf8');
          response.end();
        },
        // onError
        function(path){
          sendHttpError(response, 404,"url '"+request.url+"' not found");
        });   
    }else{
      // pas un fichier textile !
      sendHttpError(response, 500,"url '"+request.url+"' is not a Textile resource.");
    }
  }else{
    // il n'y a pas d'abonné à l'adresse demandée
    sendHttpError(response, 404,"url '"+request.url+"' not found");
  }
};

/**
 * Génération d'une page d'erreur avec status HTTP = <code>errCode</code>
 * et retourne le message <code>errMessage</code>.
 */
function sendHttpError(response, errCode,errMessage){
  response.writeHeader(errCode,{'Content-Type':"text/html"});
  content="<h1>Error "+errCode+"</h1><p class=\"error\">"+errMessage+"</p>";
  response.write(decorateHtml(content),'utf8');
  response.end();
};

/**
 * Ajoute la structure HTML au <code>content</code>.
 * @param content Contenu textile généré en HTML.
 */
function decorateHtml(content){
  var template = "<!DOCTYPE html >" 
          + "<html>" 
          + "<head>" 
          + "<meta Charset=\"utf-8\">" 
          + "<title>Demo02-1 with Static Template</title>"
          + "</head>" 
          + "<body>" 
          + "<header>" 
          + "<h1>Demo02-1</h1>"
          + "<h2>with Static Template</h2>"
          + "</header>" 
          + "<div class=\"container\">" 
          + "<div id=\"markup\">" 
          + "<div id=\"content\" class=\"markdown-body\">" 
          + "{{{content}}}" 
          + "</div>" 
          + "<footer>" 
          + "<p>&copy; 2013 - Capgemini - "
          + "<a href=\"mailto:frederic.delorme@capgemini.com\" title=\"contact author\"/>"
          + "Frédéric Delorme" 
          + "</a>"
          + "</footer>" 
          + "</body>" 
          + "</html>";
    return mustache.render(template,{'content':content});
}

// Déclaration du serveur
var server = http.createServer(myRequestHandler).listen(8000);