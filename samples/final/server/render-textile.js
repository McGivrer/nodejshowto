var textile = require('textile-js');

var renderPage = function(response, page) {
  page.content = textile(page.content).toString();
  page.title = "# " + page.url.substring(1,page.url.length-(".textile".length));
  // write page to response.
  console.info("Rendering textile Markup :[" + page.content.substring(0, 10) + "...]");
  page.toHtml(response);
};

module.exports = {
  renderPage : renderPage,
}


