var fs        = require('fs');

function loadFile(path,onSuccess,onError){
	fs.exists(path,function(exists){  
    if(!exists){
      onError(path);
    } else {
    	if(path.match("textile$")==="textile" 
          ||path.match("md$")==="md"
          ||path.match("txt$")==="txt" 
          || path.match("css$")==="css"){
        console.info("serve text based file");
    		fs.readFile(path, "utf8", function(err, data){
	    		if(err){
	    			onError(path);
	    		}else{
	    			onSuccess(path,data);
	    		}
			  });
	    }else{
        console.info("serve binary data");
	    	fs.readFile(path, function(err, data){
	    		if(err){
	    			onError(path);
	    		}else{
	    			onSuccess(path,data);
	    		}
			  });
      }
	  }
  });
};

module.exports = {
	load: loadFile, 
}