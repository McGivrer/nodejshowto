var marked    = require('marked'),
    highlight = require('highlight');

/**
 * Configuration for marked
 */
marked.setOptions({
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  langPrefix: 'lang-',
  highlight: function(code, lang) {
    if (lang === 'js') {
      return highlight.javascript(code);
    }
    /*else if (lang === 'xml') {
      return highlight.xml(code);
    }*/
    else if (lang === 'java') {
      return highlight.java(code);
    }
    return code;
  }
});

var renderPage = function(response, page) {
  // retrieve first H1 title page.
  var treelex = marked.lexer(page.content);
  page.title = treelex[0].text;

  // convert to HTML content page.
  page.content = marked(page.content);

  // write page to response.
  page.toHtml(response);
};


module.exports = {
  renderPage : renderPage,
}

 