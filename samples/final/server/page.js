var sys = require('sys'),
  path = require('path'),
  fs = require('fs'),
  Mustache = require('mustache'),
  gravatar = require('./gravatar'),
  file = require('./loadfile');

/**
 * Simple object for a web page.
 */
var Page = function(server, title, content, template) {
  /**
   * served by 
   */
  this.server = server;
  /** Page title*/
  this.title = title;
  /** Content of the page */
  this.content = content;
  /** stylesheet to use with this page */
  this.stylesheet = "css/doc.css";
  this.url = "";
  this.filepath = "";
  // template Mustache to generate page.
  this.template = template;
  this.theme = "default";

  // Set the defautl author.
  this.author = {
    "username":"",
    "firstname":"",
    "lastname":"",
    "email":"",
  };
  this.author.name = ""+this.author.firstname+" "+this.author.lastname;
  this.author.gravatar = gravatar.get(this.author.email,16);

  /**
   * Set the default author.
   * @param author structure = {
   * "username":"McGivrer",
   * "firstname":"Frédéric",
   * "lastname":"Delorme",
   * "email":"frederic.delorme@capgemini.com",
   * "name" : "Frédéric Delorme",
   * "gravatar" : "" //md5 encoded unique ID for gravatar
   * };
   *
   */
  this.setAuthor = function(author){
    this.author = author;
  }

  /**
   * Generate CSS link.
   */
  this.includeCSS = function() {
    var cssListToHtml = "";
    this.stylesheet = (this.stylesheet.endsWith(",") ? this.stylesheet : this.stylesheet + ",")
    var csslist = this.stylesheet.split(",");
    for (css in csslist) {
      if (csslist[css] != "") {
        cssListToHtml += Mustache.to_html(
                            "<link rel=\"stylesheet\" type=\"text/css\" "
                            +"href=\""+"/wiki/themes/"+serverOptions.theme+"/{{{css}}}\" />", {
          "css": csslist[css]
        });;
      }
    }
    return cssListToHtml;
  }

  /**
   * Initialise HTML content with default values
   */
  this.useDefaultHtmlTemplate = function(page) {
    return "<!DOCTYPE html >" 
          + "<html>" 
          + "<head>" 
          + "<meta Charset=\"utf-8\">" 
          + "<title>" + page.title + "</title>" 
          + page.includeCSS() 
          + "</head>" 
          + "<body>" 
          + "<header>" 
          + "</header>" 
          + "<div class=\"container\">" 
          + "<div id=\"markup\">" 
          + "<div id=\"content\" class=\"markdown-body\">" 
          + page.content 
          + "</div>" 
          + "<footer>" 
          + "<p>&copy; 2013 - "
          + "<a href=\"mailto:frederic.delorme@capgemini.com\" title=\"contact author\"/>"
          + "Frédéric Delorme&nbsp;" 
          + "</a>"
          + "<img class=\"avatar\" src=\"" + gravatar.get("frederic.delorme@gmail.com", 24) + "\"" + " alt=\"gravatar avatar\"/>" 
          + "</p>" 
          + "</footer>" 
          + "</body>" 
          + "</html>";
  }

  /**
   * Create HTML page with all attributes.
   */
  this.toHtml = function(response) {
    var page = this;
    console.log("generate:[" + page.title + "]");
    console.log("page:"+page.content.substring(0,10)+"...");
    var tpmPath = ( page.theme ?
                    path.join(process.cwd(), "/wiki/themes/"+page.theme+"/"+page.template):
                    path.join(process.cwd(), "/wiki/"+page.template)
                  );
    
    if (page.template != "") {
      file.load(
        tpmPath,
        // on Success
        function(path,dataTemplate){
          console.info("rendrering page with template [" + dataTemplate.toString() + "].");
          var template = dataTemplate.toString();
          var html = Mustache.render(template,page);

          response.writeHeader(200, {"Content-Type": "text/html"});
          response.write(html, 'utf8');
          response.end();
        },
        
        // onError
        function(path){
          console.error("template [" + page.template + "] not found. Use defautl HTML5 one.");

          response.writeHeader(200, {"Content-Type": "text/html"});
          response.write(page.useDefaultHtmlTemplate(page), 'utf8');
        }
      )
    }else{
      console.info("no template.")
      response.writeHeader(200, {"Content-Type": "text/html"});
      response.end(page.useDefaultHtmlTemplate(page));
    }
  }

  return this;
};

module.exports = {
  create: Page
}