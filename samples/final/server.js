/**
 * Documents Wiki oriented Server.
 * based on NodeJS server and related modules, 
 * the <code>DocServer</code> is able to
 * serve Markdown and textile document, and obviously HTML, 
 * CSS and other images resources
 *
 * @author Frédéric Delorme<frederic.delorme@capgemini.com>
 * @since 2013
 * @see http://nodejs.org
 * @see http://redcloth.org/hobix.com/textile/
 * @see http://daringfireball.net/projects/markdown/syntax
 */
/**
 * Dependencies
 * @see package.json
 */ 
var http         = require('http'),
  url            = require('url'),
  sys            = require('sys'),
  path           = require('path'),
  Page           = require('./server/page'),
  file           = require('./server/loadfile'),
  gravatar       = require('./server/gravatar'),
  MarkdownRender = require('./server/render-markdown'),
  TextileRender  = require('./server/render-textile');

/**
 * default author
 */
var authors = { 
  "fde":{
    "username"  : "mcgivrer",
    "firstname" : "Frédéric",
    "lastname"  : "Delorme",
    "email"     : "frederic.delorme@capgemini.com",
    "avatar"    : gravatar.get("frederic.delorme@capgemini.com",16),
    "name"      : "Frédéric Delorme",
    },
  "gse":{
    "username"  : "gscheibel",
    "firstname" : "Guillaume",
    "lastname"  : "Scheibel",
    "email"     : "guillaume.scheibel@capgemini.com",
    "avatar"    : gravatar.get("guillaume.scheibel@capgemini.com",16),
    "name"      : "Guillaume Scheibel",
    },
};

/**
 * Menu definition
 */
var menu = [
  { 'url':"/wiki/index.textile", 'title':"Aller à l'accueil du wiki", 'key':"A", 'label':"Accueil" },
  { 'url':"/wiki/server.textile", 'title':"Aller à la sopecification du serveur", 'key':"S", 'label':"Serveur" },
  { 'url':"/wiki/client.textile", 'title':"Aller à la spécification des clients", 'key':"C", 'label':"Clients" },
  { 'url':"/wiki/data.textile", 'title':"Parcourir le descriptif des structures de données", 'key':"D", 'label':"Data" },
];

/**
 * News (versioning)
 */
var news = [
  { 'id':"1", 'title':"Server ON", 'content':"server corrigé et opérationnel", 'author':"fdelorme", 'date':"30/04/2013"},
  { 'id':"2", 'title':"Template !", 'content':"Template opérationnel dans toutes les pages", 'author':"fdelorme", 'date':"30/04/2013"},
];

/**
 * overload String objecty with startsWith method.
 */
String.prototype.startsWith = function(prefix) {
  return this.indexOf(prefix) === 0;
}

/**
 * overload String object with endsWith method.
 */
String.prototype.endsWith = function(suffix) {
  return this.match(suffix + "$") == suffix;
};

// set the server options.
serverOptions = {
  encoding:"utf-8",
  theme: "default",
  index: "/wiki/index.textile",
  renders:  { 
              "md": { ext: ".md", render: MarkdownRender},
              "textile" : { ext:".textile", render: TextileRender}
            },
  templatePage: "template.html",
  templateError: "error.html",
};

/**
 * Http Server Request Handler
 * @param request
 * @param response
 */
var ServerDoc = function(request, response) {
  var self = this;



  var urlPath = url.parse(request.url).pathname;

  if (urlPath == "/") {

    // redirect to the wiki Home page
    response.writeHead(302, {
      'Location': serverOptions.index
    });
    response.end();
    console.info("Redirect url to '/wiki/index.textile'");

  } else {
    // serve requested url
    var filePath = path.join(process.cwd(), urlPath);

    console.info("serve url to '" + urlPath + "'");

    file.load(
      // file to serve.
      filePath,

      // onSuccess
      function(path, data) {
        self.servePage(path,urlPath, data, response,request);
      },

      // onError
      function(path) {
        self.sendHttpError(
        response,
        404,
          "Page for url: <code>" + path + "</code> does not exist !");
      }
    );
  }

  /**
   * Serve Page for request.
   */
  this.servePage = function(path, urlPath, data, response,request){

    console.info("Read file:" + path);

    var done = false;
    

    
    // detect extension for the requested file.
    var ext = path.substring(path.indexOf('.')+1,path.length);
    
    // extract right renderer for this kind of file (if render exists).
    if(serverOptions.renders[ext]){
      var render = serverOptions.renders[ext].render;
      // prepare page to rend.
      var myPage = Page.create(self, "", "", serverOptions.templatePage);
      myPage.url = request.url;
      myPage.filepath = path;
      myPage.theme = serverOptions.theme;
      myPage.content = data.toString();
      myPage.menu = menu;
      myPage.news = news;
      myPage.author =  authors['fde'];

      // prepare response header.
      response.writeHeader(200, {
        "Content-Type": "text/plain",
      });

      // Dynamic call to rendering markup pipeline
      render.renderPage(response,myPage);
      console.info("Rendered page with "+render);

    }else{
    
      // If not, Just serve the file.
      console.info("Serve file for :" + urlPath);
      server.serveFile(response, path, data);      
    
    }

  };

  /**
   * Send a formated HTTP Server Error <code>errCode</code> with
   * <code>message</code> to <code>response</code>.
   *
   * @param response The http response where to write error page.
   * @param errCode the HTTP Error code.
   * @param message the string for the error message.
   */
  this.sendHttpError = function(response, errCode, message) {
    var err = Page.create(
      "Server Error (code:" + errCode + ")",
      "<h1>Error</h1>\n<h2>Error " + errCode + "</h2>\n<p>" + message + "</p>",
      "wiki/template.html");
    // write page to response.
    err.toHtml(response);
  }

  /** 
   * Send <code>data</code> from file <code>filepath</code> as
   * binary content to <code>response</code>. if <code>filepath</code>
   * contains ".css", set the Header mimes type to text/css.
   *
   * @param response the Http Response where to write the data.
   * @param data content data of the filepath file.
   */
  this.serveFile=function(response, filepath, data) {

    if (filepath.endsWith(".css")) {
      response.writeHeader(200, {
        "Content-Type": "text/css"
      });
    }
    response.write(data, "binary");
    response.end();
  }
};


var server = http.createServer(ServerDoc).listen(8000);
