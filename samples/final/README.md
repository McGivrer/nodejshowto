# Docs Server

This small nodejs server is an attempt to provide small and easy documentation server to provide highly readable documents through web.

Based on http, mustache, marked and textile-js parsers, the server will be ran with the following command line:

```shell
$> npm install
$> node server.js
```

> *Note*
>
> In case of issue running server.js because au lack of dependenxcies, try a `npm install -g` to install some dependencies globaly against local mode.

and access the [http://localhost:8000/](http://localhost:8000/) url.

> *Note for developers:*
>
> If you already had installed the `node-inspector`, you would have been able to debug our `server.js`. 
> Just run the command line bellow and access the url [http://localhost:8080/](http://localhost:8080/) to access inspector debug mode.
> The communication port used between __NodeJS__ and __node-inspector__ is th `5858`.

```shell
$> node --debug --debug-brk server.js && node-inspector
[1] 7308
debugger listening on port 5858
   info  - socket.io started
visit http://0.0.0.0:8080/debug?port=5858 to start debugging
```

Your favorite browser would display a page like the one bellow :

![figure 1 - The Node Inspector](https://bitbucket.org/capsamples/projectx/raw/b803ae636358d90e294a286f65be312b878c8034/docs/wiki/images/captures/node%20inspector.jpg)

_figure 1 - Node-Inspector module_