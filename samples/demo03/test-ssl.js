const fs   = require("fs"),
      http = require("https");
// lecture de la clé privée
var privateKey = fs.readFileSync('ssl/privatekey.pem').toString();
// LEcture du certificat
var certificate = fs.readFileSync('ssl/certificate.pem').toString();
// préparation des options serveur
var options = {
	key: privateKey,
	cert: certificate
}
// Handler de requête HTTPS
var reqHandler = function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('Securised Hello World\n');
}
// Création du serveur
var server = https.createServer( options, reqHandler ).listen(8000);
