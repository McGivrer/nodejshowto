var http = require('http'),
    https = require('https'),
    fs = require('fs'),
    url = require('url'),
    path = require('path'),
    mustache = require('mustache'),    
    textile = require('textile-js');

var application = {
  'title'          : "Demo03-3",
  'domaine'        : "localhost",
  'port'           : 8000,
  'sslPort'        : 8443,
  'path'           : {  'css'   :"/resources/css",
                        'js'    : "/resources/js",
                        'image' : "/resources/images",
                        'public': "/wiki",
                        'secured':"/secured"},
  'homePage'       : "index.textile",
  'template'       : {  'page'  : "template.html",
                        'error' : "error.html"},
  'encoding'       : "utf8",
  'routes'         : {
    "http":{ 
      'wiki'      : { path: "/wiki",      handler: myHttpRequestHandler},
      'resources' : { path: "/resources", handler : myHttpRequestHandler}},
    "https":{
      'secured'   : { path: "/secured",   handler: myHttpsRequestHandler},
      'resources' : { path: "/resources", handler: myHttpRequestHandler}},
  },
  'renders'        : {
    'css'     : { ext: 'css',               render: sendFile},
    'js'      : { ext: 'js',                render: sendFile},
    'textile' : { ext: 'textile|txtl',      render: renderTextile},
    'markdown': { ext: 'md|mdown|markdown', render: renderMarkdown},
    },
  'templates'    : {
      'html':{ ext:'html', parse : mustacheParser },
    },
  'url' : { 'public': function(){return "http://"+application.domaine+":"+application.port+application.path.public;},
            'secured' : function(){return "https://"+application.domaine+":"+application.sslPort+application.path.secured;}
          }
};

/**
 * Chargement d'un fichier <code>filePath</code>
 */
function loadFile(filePath,onSuccess,onError){

  fs.exists(filePath,
    function(exists){  
      if(!exists){
        onError(filePath);
      } else if(filePath.match("css$")     == "css"     ||
                filePath.match("js$")      == "js"      ||
                filePath.match("textile$") == "textile" ||
                filePath.match("html$")    == "html" ){

        fs.readFile(filePath, application['encoding'], function(err, data){
          if(err){
            onError(filePath);
          }else{
            onSuccess(filePath,data);
          }
        });

      }else{

        fs.readFile(filePath, function(err, data){
          if(err){
            onError(filePath);
          }else{
            onSuccess(filePath,data);
          }
        });

      }
    });
};

// Server request handler
function myHttpRequestHandler(request,response){
  requestHandler("http", application.path.public, request,response);
};

// Server request handler
function myHttpsRequestHandler(request,response){
  requestHandler("https",application.path.secured, request,response);
};

// Server request handler
function requestHandler(protocole, dirpath, request,response){
  // si la page cherché est bien dans le path wiki
  var urlPath = url.parse(request.url).pathname;
  // si le fichier est bien dans /wiki
  if(urlPath =="/"){
    // redirect to the wiki Home page
    var location=protocole  + "://"
                            + application.domaine
                            + (application.port!=(protocole=="https"?443:80)?":"+application.port:"")
                            + dirpath
                            + "/"
                            + application.homePage;
    redirectTo(response, location);
  } else if((urlPath.indexOf(dirpath)       === 0) 
        || (urlPath.indexOf("/favicon.ico") === 0) 
        || (urlPath.indexOf("/resources")   === 0) ) {
    // on procède à son chargement
    var txtFilePath = path.join(process.cwd(), urlPath);
    // Si le fichier demandé est bien une resource textile
    
    loadFile( txtFilePath, 

      // onSuccess
      function(filepath,data){

        var ext = filepath.substring(filepath.indexOf('.')+1,filepath.length);
    
        // extract right renderer for this kind of file (if render exists).
        if(application.renders[ext]){
          var engine = application.renders[ext];
          
          engine.render(dirpath, response, data);

        }else{
          // pas un fichier textile !
          sendFile(filepath, response, data);
        }

      },

      // onError
      function(filepath){
        sendHttpError(filepath,response, 404,"url '"+request.url+"' not found (no file '"+filepath+"').");
      });   
  }else{
    // il n'y a pas d'abonné à l'adresse demandée
    sendHttpError(dirpath,response, 404,"Nothing to produce for the url '"+request.url+"' !");
  }
};

/**
 * Génération d'une page d'erreur avec status HTTP = <code>errCode</code>
 * et retourne le message <code>errMessage</code>.
 */
function sendHttpError(dirpath, response, errCode,errMessage){
    var content= "";
    loadFile(path.join(process.cwd(),dirpath+"/"+application.template.error),
      function(filepath,templateError){

        var ext = filepath.substring(filepath.indexOf('.')+1,filepath.length);
    
        // extract right renderer for this kind of file (if render exists).
        if(application.templates[ext]){
          var templatizer = application.templates[ext];
          content = templatizer.parse(templateError, {'error' : { code: errCode, message : errMessage }}, application);
          response.writeHeader(errCode,{'Content-Type':"text/html"});
          response.end(content, application.encoding);
        }else{
          defaultHttpError(dirpath, response, errCode, errMessage);
        }
        console.log("Error "+errCode+": "+errMessage);
      },
      function(filepath){
        defaultHttpError(filepath, response, errCode, errMessage);
      });
};

function defaultHttpError(dirpath, response,errCode,errMessage){
  content="<h1>Error "+errCode+"</h1><p class=\"error\">"+errMessage+"</p>";
  response.writeHeader(errCode,{'Content-Type':"text/html"});
  response.end(content, application.encoding);
  console.log("Error "+errCode+": "+errMessage);
}

function errorNoTemplateFound(dirpath, response){
  defaultHttpError(dirpath,response,
                            500, 
                            "No templatizer found for this extension file.");
}
/**
 * Ajoute la structure HTML au <code>content</code>.
 * @param content Contenu textile généré en HTML.
 */
function decorateHtml(dirpath, response, content){

    loadFile(path.join(process.cwd(),dirpath+"/"+application.template.page),
      function(templateFilePath,template){
        console.info("decorateHtml:templateFilePath=["+templateFilePath+"]");
        templatizePage( template,templateFilePath,
                        response,content,
                        errorNoTemplateFound);

      },
      function(templateFilePath){
        // la réponse contiendra du HTML
        response.writeHeader(500,{'Content-Type':"text/html"});
        console.error("decorateHtml:templateFilePath not found ["+templateFilePath+"]");
       
        response.write("<h1>Error 500</h1>"
          +"<p class=\"\">Template ["+templateFilePath+"] not found !</p>"
          + "<div class=\"container\"><div id=\"markup\"><div id=\"content\" class=\"markdown-body\">"
          + content
          + "</div></div></div>", application.encoding);
        response.end();
      }); 
}

/**
 * Detect the right templatizer to teplatize the page in the rendering pipe.
 * parse the <code>application.teplates</code> map to find the corresponding templatizer 
 * for the extention file.
 */
function templatizePage(template, templateFilePath, response, content, onError){
          // extract right templatizer for this kind of template file (if templatizer exists).

  var ext = templateFilePath.substring(templateFilePath.indexOf('.')+1,templateFilePath.length);
  console.info("Try to retrieve the templatizer for '"+ext+"' extension template file");
  if(application.templates[ext]){
    var templatizer = application.templates[ext];
    // la réponse contiendra du HTML
    response.writeHeader(200,{'Content-Type':"text/html"});
    content = templatizer.parse(template, content, application);
    console.log("content temlpatized: ["+content+"]");
    response.end(content, application.encoding);
  }else{
    onError(dirpath,response);
  }
}
/**
 * render textile markup pages
 */
function renderTextile(filepath, response, data){
  // on envoie la page générée sur la réponse HTTP
  decorateHtml(filepath, response, textile(data).toString());
}

/**
 * render markdown markup pages
 */
function renderMarkdown(filepath, response, data){
  // TODO Implement the markdown renderer.
  decorateHtml(filepath, response, data);
}

/**
 * Mustache Template engine
 */
function mustacheParser(template, content, application){
  return mustache.render(template,
                {'content': mustache.render( content, {'app':application}),
                  'app': application }, application.encoding);
}

/** 
 * Send <code>data</code> from file <code>filepath</code> as
 * binary content to <code>response</code>. if <code>filepath</code>
 * contains ".css", set the Header mimes type to text/css.
 *
 * @param response the Http Response where to write the data.
 * @param data content data of the filepath file.
 */
function sendFile(filepath,response, data) {

  if (filepath.match("css$") == "css") {
    response.writeHeader(200, { "Content-Type": "text/css" });
    response.end(data, application.encoding);
  }else if(filepath.match("png$") == "png"){
    response.writeHeader(200, { "Content-Type": "image/png" });
    response.end(data, "binary");
  }else{
    response.end(data, "binary");
  }
}

/**
 * Redirection to <code>location</code>.
 */
function redirectTo(response, location){
  response.writeHead(302, {
    'Location': location
  });
  console.log("redirect to home: "+location);
  response.end();
}

// lecture de la clé privée
var privateKey = fs.readFileSync(path.join(process.cwd(),'ssl/privatekey.pem')).toString();
// Lecture du certificat
var certificate = fs.readFileSync(path.join(process.cwd(),'ssl/certificate.pem')).toString();
// préparation des options serveur
var options = {
  key: privateKey,
  cert: certificate
}
// Déclaration du serveur
//port = process.env.PORT || 5000;
console.log("Application :"+application.title+"\n"+application.toString());

var server1 = http.createServer(myHttpRequestHandler).listen(application.port);
var server2 = https.createServer(options,myHttpsRequestHandler).listen(application.sslPort);
